package StepDefinition;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class helloWorld {

    @When("user run the script")
    public void user_run_the_script() {
        System.out.println("User run the maven hello world test case.");
    }
    @Then("Hello World will show in the console")
    public void hello_world_will_show_in_the_console() {
        System.out.println("HELLO WORLD!!!");
    }
}
