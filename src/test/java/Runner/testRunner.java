package Runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(value = Cucumber.class)
@CucumberOptions(
    features = "src/test/resources/", glue = "StepDefinition", monochrome = true,
        plugin = {"pretty", "junit:target/JUnitReports/report.xml",
        "json:target/JSONReports/report.json",
        "html:target/HTMLReports/report.html",
        "rerun:target/Failed Reports/failed_scenarios.txt"
        }
)
public class testRunner {
}
